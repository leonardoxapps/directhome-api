require('./src/config/mongoose')();
const aclHelper = require('./src/helpers/acl');
const logger = require('./src/helpers/logger');
const loggerMiddleware = require('koa-logger');
const corsError = require('koa-cors-error');
const api = require('./src/config/api');
const bodyParser = require('koa-body');
const mount = require('koa-mount');
const gzip = require('koa-gzip');
const cors = require('koa-cors');
const jwt = require('koa-jwt');
const http = require('http');
const koa = require('koa');
const db = require('./db');

const server = new koa();
aclHelper.init();
const accountRouter = require('./src/modules/users/routers/account.router')
const imovelRouter = require('./src/modules/imovel/routers');


server.use(corsError);
server.use(bodyParser());
server.use(gzip());
server.use(loggerMiddleware());
server.use(cors({
    origin: '*',
    methods: 'GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS',
    headers: ['Content-Type', 'Authorization', 'nome', 'descricao', 'tipo']
}));


/* PUBLIC ROUTES */
server.use(mount('/api', accountRouter.routes()));
server.use(mount('/api', imovelRouter.routes()));

server.use(jwt({ secret: 'shared-secret' }));

/* PRIVATE ROUTES */

/* START SERVER */
db.connection.on('connected', () => {
    http.createServer(server.callback()).listen(api.port, () => {
        logger.log(`Server listening at http://localhost:${api.port}/api/`)
    })
})