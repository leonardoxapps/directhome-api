const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const ImovelSchema = new Schema({
    transacao: [],
    tipo: {
        type: String
    },
    userId:{
        type:Schema.Types.ObjectId,
        ref:'User'
    },
    endereco:[{
        logradouro:{type:String},
        numero:{type:String},
        complemento:{type:String},
        bairro:{type:String},
        cidade:{type:String},
        estado:{type:String},
        cep:{type:String},
        latitude:{type:String},
        longitude:{type:String}
    }],
    valor:{
        type: Schema.Types.Double
    },
    qtdQuartos:{type:Number},
    qtdSuites:{type:Number},
    qtdVagas:{type:Number},
    metragem:{type:Number},
    contato:{type:String},
    informacoes:{type:String},
    foto:{type:String}
}, { collection: 'imoveis' });

mongoose.model('Imovel', ImovelSchema);