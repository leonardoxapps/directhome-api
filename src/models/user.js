const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')

const Schema = mongoose.Schema

const UserSchema = new Schema({
    nome: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    hash: {
        type: String,
    },
    companyRole: {
        type: String,
    },
    systemRole: {
        type: String,
        enum: [
            'administrador',
            'usuario',
        ],
    },
    resetarsenha: {
        type: Boolean,
        default: false,
    },
    active: {
        type: Boolean,
        default: true,
    }
})

UserSchema.methods.generateHash = password => bcrypt.hashSync(password, bcrypt.genSaltSync(8), null)
UserSchema.methods.validPassword = (hash, password) => bcrypt.compareSync(password, hash)

mongoose.model('User', UserSchema)
