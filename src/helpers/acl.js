const aclInit = require('../config/acl')
const NodeACL = require('acl')

const acl = new NodeACL(new NodeACL.memoryBackend())

const getRoles = () => [
    'administrador',
    'usuario',
]


const getPermissionsTable = function* () {
    const teste = yield getRoles().map(role => ({ [role]: acl.whatResources(role) }))

    return teste.reduce((acc, roleObject) => {
            const role = Object.keys(roleObject)[0]
            acc[role] = Object.keys(roleObject[role])
            return acc;
}, {})
}

module.exports = {
    acl,
    getRoles,
    getPermissionsTable,
    init: aclInit(acl),
}
