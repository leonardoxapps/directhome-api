const controller = require('../controllers/account.controller');
const Router = require('koa-router');
const jwt = require('koa-jwt')

const router = new Router();
const baseUrl = '/users/';

router.post(`${baseUrl}login`, controller.authUser);
router.post(`${baseUrl}create`, controller.createAccount)

module.exports = router;