const hash      = require('../../../helpers/hash');
const mongoose  = require('mongoose');
const crypto    = require('crypto');
const moment    = require('moment');
const jwt       = require('koa-jwt');
const acl       = require('../../../helpers/acl').acl;
const aclInit   = require('../../../helpers/acl').init;
const Users     = mongoose.model('User');

moment.locale('pt-br');

const authUser = function* () {
    const params = this.request.body;
    const user = yield Users.findOne({ email: params.email }).lean();
    if (!user) this.throw(404, 'Usuário não encontrado');

    const token = { value: jwt.sign(user, 'shared-secret') };
    this.body = (yield hash.compare(params.password, user.password)) ?
        token :
        this.status=404;
}

const createAccount = function* () {
    const newUser = this.request.body;
    console.log(newUser);
    const user = yield Users.findOne({ email: newUser.email });
    if (user) this.throw(400, 'Usuário já existe');

    const password = yield hash.encrypt(newUser.password);
    newUser.password=password;

    newUser.systemRole  = 'usuario';

    const createdUser = yield Users.create(newUser);
    this.status=200;
};

module.exports = {
    authUser,
    createAccount
}