const Router = require('koa-router');
const controller = require('../controller');
const router = new Router();
const baseUrl = '/Imoveis/';

router.post(`${baseUrl}create`, controller.create);
router.post(`${baseUrl}search`, controller.search);
router.post(`${baseUrl}delete`, controller.del);
router.post(`${baseUrl}details`, controller.details);
router.get(`${baseUrl}list`, controller.list);

module.exports = router