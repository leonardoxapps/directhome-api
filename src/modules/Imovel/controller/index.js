const mongoose = require('mongoose');
const Imovel = mongoose.model("Imovel");
var path = require('path');
var formidable = require('formidable');
var fs = require('fs');

const create = function* () {
    params = this.request.body;

    var form = new formidable.IncomingForm();

    form.multiples = true;
    form.uploadDir = "/fotos"

    form.on('file', function(field, file) {
        fs.rename(file.path, path.join(form.uploadDir, file.name));
    });

    form.on('error', function(err) {
        this.body = {Erro: "Erro ao salvar foto"}
    });

    const imovel = yield Imovel.create({
        userId: params.userId,
        transacao: params.transacao,
        tipo: params.tipo,
        qtdQuartos: params.qtdQuartos,
        qtdSuites: params.qtdSuites,
        qtdVagas: params.qtdVagas,
        metragem: params.metragem,
        endereco: params.endereco,
        valor: params.valor,
        contato: params.contato,
        informacoes: params.informacoes,
        foto: params.foto,
    });

    this.status = 200;
    this.body = imovel;
};

const search = function* () {
  params = this.request.body;
  const imoveis = yield Imovel.find({
      transacao: params.transacao,
      tipo: params.tipo,
      qtdQuartos: params.qtdQuartos,
      qtdSuites: params.qtdSuites,
      qtdVagas: params.qtdVagas,
      metragem: { $lte: params.metragem },
  });
  this.status = 200;
  this.body = imoveis;
};

const del = function* () {
    params = this.request.body;
    yield Imovel.find({ _id:params._id }).remove().exec()
    this.status = 200;
    this.body = {Mensagem: "Ok"}
};

const list = function* () {
    const imoveis = yield Imovel.find();
    this.status = 200;
    this.body = imoveis;
}

const details = function* () {
    params = this.request.body;
    const imovel = yield Imovel.find({ _id:params._id });
    this.status = 200;
    this.body = imovel;
};

module.exports = {
    create,
    search,
    del,
    details,
    list
};