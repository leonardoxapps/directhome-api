const mongoose = require('mongoose')

const Users = mongoose.model('User')

const aclSeed = [{
    name: 'administrador',
    areas: ['signup_createAccount', 'users_getUsers', 'users_getUser'],
}, {
    name: 'usuario',
    areas: ['signup_createAccount', 'users_getUsers', 'users_getUser'],
}]

module.exports =
    acl =>
() => {
    aclSeed.forEach(role => acl.allow(role.name, role.areas, '*'))

    return Users
            .find().lean().exec((err, users) => users.forEach((user) => {
            if (!user.systemRole) return
    acl.addUserRoles(user.email, user.systemRole)
}))
}
