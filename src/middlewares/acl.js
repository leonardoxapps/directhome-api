const acl = require('../helpers/acl').acl

module.exports = area =>
function* (next) {
    const user = this.state.user;
    const isAllowed = yield acl.isAllowed(user.email, area, '*');

    this.assert(isAllowed, 403, 'Acesso negado')

    yield next
}
